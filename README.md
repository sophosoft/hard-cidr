# Hard CIDR

CIDR calculation functions to simplify complex network maps.  Heavily influenced by the functions available in Terraform.  However, these only support IPv4.  IPv6 might be supported later if there is sufficient demand.

## install

```sh
npm i @sophosoft/hard-cidr
```

## RFC 1918

An enum is provided for the RFC 1918 private address ranges.  These are best used when calculating an enterprise network map of non-overlapping subnets across multiple data centers, accounts, regions, and/or availability zones.

```ts
import { RFC_1918 } from "@sophosoft/hard-cidr"

RFC_1918.CLASS_A // 10.0.0.0/8
RFC_1918.CLASS_B // 172.16.0.0/12
RFC_1918.CLASS_C // 192.168.0.0/16
```

## functions

### cidrsubnet

Calculate a single subnet from a given CIDR range and additional bits, with an optional index.

```ts
import { cidrsubnet } from "@sophosoft/hard-cidr"

const subnet: string = cidrsubnet("172.16.0.0/12", 4, 2)
// 172.18.0.0/16
```

### cidrsubnets

Calculate a sequence of non-overlapping, non-gapped subnets from a given CIDR range

```ts
import { cidrsubnets } from "@sophosoft/hard-cidr"

const subnets: string[] = cidrsubnets("10.1.0.0/16", [3, 4, 2, 0])
// [10.1.0.0/19, 10.1.32.0/23, 10.1.34.0/25, 10.1.34.128/25]
```

### cidrhost

Calculate a single host ip from a given CIDR range

```ts
import { cidrhost } from "@sophosoft/hard-cidr"

const host: string = cidrhost("10.12.127.0/20", 16)
// 10.12.127.16
```
