import { cidrsubnets, cidrsubnet, cidrhost } from "../src"
import { expect } from "chai"

describe("functions", () => {
  const cidr = "10.0.0.0/8"

  describe("cidrsubnets", () => {
    it("increments subnet cidrs", () => {
      const expected = [
        "10.0.0.0/12",
        "10.16.0.0/12",
        "10.32.0.0/12",
        "10.48.0.0/12"
      ]
      const subnets = cidrsubnets(cidr, [4, 0, 0, 0])
      expected.forEach((c) => {
        expect(subnets).to.include(c)
      })
    })

    it("calcluates subnet cidrs", () => {
      const expected = [
        "10.0.0.0/22",
        "10.0.4.0/24",
        "10.0.5.0/27",
        "10.0.5.32/30"
      ]
      const subnets = cidrsubnets("10.0.0.0/21", [1, 2, 3, 3])
      expected.forEach((c) => {
        expect(subnets).to.include(c)
      })
    })

    it("properly sequences subnets", () => {
      const expected = [
        "10.1.0.0/19",
        "10.1.32.0/23",
        "10.1.34.0/25",
        "10.1.34.128/25"
      ]
      const subnets = cidrsubnets("10.1.0.0/16", [3, 4, 2, 0])
      expected.forEach((c) => {
        expect(subnets).to.include(c)
      })
    })
  })

  describe("cidrsubnet", () => {
    it("calcluates a subnet", () => {
      const subnet = cidrsubnet(cidr, 4, 3)
      expect(subnet).to.equal("10.48.0.0/12")
    })
  })

  describe("cidrhost", () => {
    it("determines the ip of a specified host", () => {
      const host = cidrhost("10.0.0.0/24", 2)
      expect(host).to.equal("10.0.0.2/32")
    })
  })
})
