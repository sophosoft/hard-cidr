import { Netmask } from "netmask"

/**
 * Private network ip address ranges
 * 
 * @see https://tools.ietf.org/html/rfc1918
 */
export enum RFC_1918 {
  CLASS_A = "10.0.0.0/8",
  CLASS_B = "172.16.0.0/12",
  CLASS_C = "192.168.0.0/16"
}

/**
 * Calculate a sequence of several consecutive address ranges for a given CIDR prefix
 * 
 * `newbits` should be in decending order to ensure no overlaps and no gaps between ranges
 * 
 * @param prefix  The starting CIDR
 * @param newbits A collection of additional prefix bits for each consecutive range
 */
export function cidrsubnets(prefix: string, newbits: number[]): string[] {
  let root: Netmask = new Netmask(prefix)
  const subnets: string[] = []

  newbits.forEach((bits: number) => {
    const subnet: Netmask = new Netmask(cidrsubnet(root.toString(), bits))
    subnets.push(subnet.toString())
    root = subnet.next()
  })

  return subnets
}

/**
 * Calculate a single subnet address range from a given CIDR prefix
 * 
 * @param prefix  The starting CIDR
 * @param newbits The number of additional bits to extend the prefix
 * @param netnum  (optional) A range index within the given CIDR
 */
export function cidrsubnet(prefix: string, newbits: number, netnum: number = 0): string {
  const root: Netmask = new Netmask(prefix)
  const subnet: Netmask = new Netmask(`${root.base}/${root.bitmask + newbits}`)
  return subnet.next(netnum).toString()
}

/**
 * Calculate a single host ip in a given range
 * 
 * @param prefix  The CIDR range
 * @param hostnum The index of the host within the range
 */
export function cidrhost(prefix: string, hostnum: number): string {
  const root: Netmask = new Netmask(prefix)
  return cidrsubnet(prefix, 32 - root.bitmask, hostnum)
}
